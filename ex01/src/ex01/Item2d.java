package ex01;

import java.io.Serializable;
/** ������ �������� ������ � ��������� ����������.
* @author xone
* @version 1.0
*/
public class Item2d implements Serializable{
	/** �������� ����������� �������. */
	private int x;
	
	/** ��������� ���������� �������. */
	private double y;
	
	/** ������������� �������� �����: ���������
	* � ���������� ���������� �������.
	* @param x - �������� ��� ������������� ���� {@linkplain Item2d#x}
	* @param y - �������� ��� ������������� ���� {@linkplain Item2d#y}
	*/
	public Item2d() {
		this.x = 0;
		this.y = 0;
	}
	
	/** ��������� �������� ���� {@linkplain Item2d#x}
	* @param x - �������� ��� {@linkplain Item2d#x}
	* @return �������� {@linkplain Item2d#x}
	*/
	public void setX(String x) {
		this.x = Integer.parseInt(x, 2);
		setY((Math.pow(getX(),2))+(((Math.sqrt(3))*Math.pow(getX(),2))/4));
	}
	
	/** ��������� �������� ���� {@linkplain Item2d#x}
	* @return �������� {@linkplain Item2d#x}
	*/
	public int getX() {
		return x;
	}
	
	/** ��������� �������� ���� {@linkplain Item2d#y}
	* @param d - �������� ��� {@linkplain Item2d#y}
	* @return �������� {@linkplain Item2d#y}
	*/
	public void setY(double d) {
		this.y = d;
	}
	
	/** ��������� �������� ���� {@linkplain Item2d#y}
	* @return �������� {@linkplain Item2d#y}
	*/
	public double getY() {
		return y;
	}
	
	/** ��������� �������� {@linkplain Item2d#x} � {@linkplain Item2d#y}
	* @param x - �������� ��� {@linkplain Item2d#x}
	* @param y - �������� ��� {@linkplain Item2d#y}
	* @return this
	*/
	public Item2d setXY(String x, String y) {
		this.x = Integer.parseInt(x, 2);
		this.y = Integer.parseInt(y, 2);
		return this;
	}
	
	/** ������������ ��������� ���������� � ���� ������.<br>{@inheritDoc} */
	@Override
	public String toString() {
	return "x = " + x + ", y = " + y;
	}
	
	/** ������������� ��������������� �����.<br>{@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item2d other = (Item2d) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		// �������� ��������� ���������� ���������� �������
		if (Math.abs(Math.abs(y) - Math.abs(other.y)) > .1e-10)
			return false;
		return true;
	}
}
