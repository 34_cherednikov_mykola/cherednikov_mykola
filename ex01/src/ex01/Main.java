package ex01;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
/** ���������� � ����������� �����������.
* �������� ���������� ������������ ������ main().
* @author xone
* @version 1.0
* @see Main#main
*/
public class Main {
	/** ������ ������ {@linkplain Calc}.<br>������ ������ ���. �������. */
	private Calc calc = new Calc();
	/** ���������� ����. */
	private void menu() {
		String s = null;
		String smt = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		do {
			do {
				System.out.println("Enter command...");
				System.out.print("'q'uit, 'v'iew, 'x'-set triangle side, 'y'-set quadrangle side, 's'ave, 'c'alculate 'r'estore: ");
				try {
					s = in.readLine();
				} catch(IOException e) {
					System.out.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			switch (s.charAt(0)) {
			case 'q':
				System.out.println("Exit.");
				break;
			case 'v':
				System.out.println("View current.");
				calc.show();
				break;
			case 'x':
				try{
					System.out.print("������� ������� ������������ � �������� ����: ");
					smt = in.readLine();
					calc.initx(smt);
					System.out.println("�������� �����������"+"\n");
					calc.show();
				} 
				catch(Exception e) {
					System.out.println("������������ ���� ��������� ����"+"\n");
				}
				break;
			case 's':
				System.out.println("Save current.");
				try {
					calc.save();
				} catch (IOException e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show();
				break;
			case 'c':
				System.out.println("Area result:"+calc.calc());
				break;
			case 'r':
				System.out.println("Restore last saved.");
				try {
					calc.restore();
				} catch (Exception e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show();
				break;
			default:
				System.out.print("Wrong command. ");
			}
		} while(s.charAt(0) != 'q');
	}
	/** ����������� ��� ������� ���������.
	 * ����������� �������� ������� ��� ��������� ����������.
	 * ���������� ���������� ��������� �� �����.
	 * @param args - ��������� ������� ���������.
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.menu();
	}
}