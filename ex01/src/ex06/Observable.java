package ex06;
import java.util.HashSet;
import java.util.Set;
/** ���������� ��������
* ��������������
* ������������
* � �����������;
* ������ Observer
* @author xone
* @version 1.0
* @see Observer
*/
public abstract class Observable {
/** ��������� ������������; ������ Observer
* @see Observer
*/
private Set<Observer> observers = new HashSet<Observer>();
/** ��������� �����������; ������ Observer
* @param observer ������-�����������
*/
public void addObserver(Observer observer) {
observers.add(observer);
}
/** ������� �����������; ������ Observer
* @param observer ������-�����������
*/
public void delObserver(Observer observer) {
observers.remove(observer);
}
/** ��������� ������������ � �������; ������ Observer
* @param event ���������� � �������
*/
public void call(Object event) {
for (Observer observer : observers) {
observer.handleEvent(this, event);
}
}
}