package ex07;

import java.awt.Dimension;
import ex02.ViewResult;

/**
 * ConcreteProduct (������ �������������� Factory Method); ����������� �������
 * 
 * @author xone
 * @version 1.0
 * @see ViewResult
 * @see Window
 */
public class ViewWindow extends ViewResult {
	/** ���������� ��������� �������� */
	private static final int POINTS_NUM = 100;
	/** ������������ ���� */
	private Window window = null;

	/** �������� � ����������� ���� */
	public ViewWindow() {
		super(POINTS_NUM);
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Result");
		window.setVisible(true);
	}

	@Override
	public void viewInit() {
		init((int) (Math.random() * 1000.0));
	}

	@Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}