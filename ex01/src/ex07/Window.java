package ex07;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import ex01.Item2d;
import ex02.ViewResult;

/**
 * �������� ���� ����������� �������
 * 
 * @author xone
 * @version 1.0
 * @see Frame
 */
@SuppressWarnings("serial")
public class Window extends Frame {
	/** ������� ������� �� ���� ���� */
	private static final int BORDER = 20;
	/**
	 * ������, ����������� ��������� {@linkplain ex02.View};<br>
	 * ����������� ��������� �������� {@linkplain ex01.Item2d}
	 */
	private ViewResult view;

	/**
	 * �������������� {@linkplain Window#view};<br>
	 * ������� ���������� ������� �������� ����
	 * 
	 * @param view �������� ��� ���� {@linkplain Window#view}
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
//System.exit(0);
				setVisible(false);
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER;
		r.y += 25 + BORDER;
		r.width -= r.x + BORDER;
		r.height -= r.y + BORDER;
		c.x = r.x;
		c.y = r.y + r.height / 2;
		c.width = r.width;
		c.height = r.height / 2;
		g.setColor(Color.LIGHT_GRAY);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		double maxX = 0, maxY = view.getItems().get(0).getY(), scaleX, scaleY;
		for (Item2d item : view.getItems()) {
			if (item.getX() > maxX)
				maxX = item.getX();
			if (Math.abs(item.getY()) > maxY)
				maxY = Math.abs(item.getY());
		}
		g.drawString("+" + maxY, r.x, r.y);
		g.drawString("-" + maxY, r.x, r.y + r.height);
		g.drawString("+" + maxX, c.x + c.width - g.getFontMetrics().stringWidth("+" + maxX), c.y);
		scaleX = c.width / maxX;
		scaleY = c.height / maxY;
		g.setColor(Color.BLUE);
		for (Item2d item : view.getItems()) {
			g.drawOval(c.x + (int) (item.getX() * scaleX) - 5, c.y - (int) (item.getY() * scaleY) - 5, 10, 10);
		}
	}
}