package ex07;

import ex02.View;
import ex02.Viewable;

/**
 * ConcreteCreator (������ �������������� Factory Method); ��������� �����,
 * "�����������" �������
 * 
 * @author xone
 * @version 1.0
 * @see Viewable
 * @see Viewable#getView()
 */
public class ViewableWindow implements Viewable {
	@Override
	public View getView() {
		return new ViewWindow();
	}
}