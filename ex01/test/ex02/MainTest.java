package ex02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;
import java.io.IOException;
import ex01.Item2d;

/** ��������� ������������
* ������������� �������.
* @author xone
* @version 2.0
*/
public class MainTest {
	/** �������� �������� ���������������� ������ {@linkplain ViewResult} */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult(5);
		Item2d item = new Item2d();
		int ctr = 0;
		item.setX("0");
		view.init(0);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",view.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("1");
		view.init(1);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",view.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("10");
		view.init(1);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
				view.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("110");
		view.init(2);
		assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
				view.getItems().get(ctr).equals(item));
		ctr++;
	}
	/** �������� ������������. ������������ �������������� ������. */
	@Test
	public void testRestore() {
		ViewResult view1 = new ViewResult(1000);
		ViewResult view2 = new ViewResult();
		// �������� �������� ������� �� ��������� ����� ���������� ���������
		int val = (int) (Math.random()*100.0);
		view1.init(val);
		// �������� ��������� view1.items
		try {
			view1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		// �������� ��������� view2.items
		try {
			view2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		// ������ ��������� ������� �� ���������, ������� ���������
		assertEquals(view1.getItems().size(), view2.getItems().size());
		// ������ ��� �������� ������ ���� �����.
		// ��� ����� ����� ���������� ����� equals
		assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
	}
}