package ex03;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;
import java.io.IOException;
import ex01.Item2d;
/** ��������� ������������
* ������������� �������.
* @author xone
* @version 3.0
*/
public class MainTest {
	/** �������� �������� ���������������� ������ {@linkplain ViewTable} */
	@Test
	public void testCalc() {
		ViewTable tbl = new ViewTable(10, 5);
		assertEquals(10, tbl.getWidth());
		assertEquals(5, tbl.getItems().size());
		tbl.init(0);
		Item2d item = new Item2d();
		int ctr = 0;
		item.setX("0");
		assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
				tbl.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("1");
		tbl.init(1);
		assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
				tbl.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("10");
		tbl.init(1);
		assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
				tbl.getItems().get(ctr).equals(item));
		ctr++;
		item.setX("110");
		tbl.init(2);
		assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
				tbl.getItems().get(ctr).equals(item));
		ctr++;
	}
	/** �������� ������������. ������������ �������������� ������. */
	@Test
	public void testRestore() {
		ViewTable tbl1 = new ViewTable(10, 1000);
		ViewTable tbl2 = new ViewTable();
		// �������� �������� ������� �� ��������� ����� ���������� ���������
		int val = (int) (Math.random()*100.0);
		tbl1.init(val);
		// �������� ��������� tbl1.items
		try {
			tbl1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		// �������� ��������� tbl2.items
		try {
			tbl2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		// ������ ��������� ������� �� ���������, ������� ���������
		assertEquals(tbl1.getItems().size(), tbl2.getItems().size());
		// ������ ��� �������� ������ ���� �����.
		// ��� ����� ����� ���������� ����� equals
		assertTrue("containsAll()", tbl1.getItems().containsAll(tbl2.getItems()));
	}
}