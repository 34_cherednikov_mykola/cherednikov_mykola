package ex01;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import ex01.Calc;
import ex01.Item2d;


/** ��������� ������������ ������������� �������.
* @author Andrei
* @version 1.0
*/
public class MainTest {
	
/** �������� ����� ��� ���������� �����*/
	private static final String FNAME = "testfile.bin";
/** ��������� ����� ��� ����������� */
	private static int val = (int)Math.random() * 10000;
	
/** �������� �������� ���������������� ������ {@linkplain Calc} */
	@Test
	public void testCalc() {
		Calc calc = new Calc();
		
		calc.initx("0");
		assertEquals(calc.getResult().getX(), 0);
		calc.initx("10");
		assertEquals(calc.getResult().getX(), 2);
		calc.initx(Integer.toBinaryString(val));
		assertEquals(calc.getResult().getX(), val);
	}
/** ������������ ������� �� ���������� ����� � �������� ��������� �����*/
	@BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
		Calc calc = new Calc();
        try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
			os.writeObject(calc.getResult());
			os.flush();
			os.close();
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.toString());
        }
    }
/** �������� ��������� ����� ����� �����*/
	@AfterClass
    public static void tearDownAfterClass() throws Exception {
        // �������� �����
        new File(FNAME).delete();
    }
	
/** �������� ������������ �������������� ������. */
	@Test
	public void testRestore() {
		Item2d number = new Item2d();
		try {
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
			number = (Item2d)is.readObject();
			is.close();
			
			assertEquals(number.getX(), val);
			assertEquals(number.getY(), val);
			} catch (Exception e) {
				fail(e.getMessage());
			}
	}
	
	
}