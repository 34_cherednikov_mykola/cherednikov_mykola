package root_package.task02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/** �������� ���������� ������� ��� ���������� */
public class Quadrangle {
	/** ��������� ��������� ����������. */
	private float Area;
	/** ������ ������. */
	private Quadrangle_side qs;
	/** �������������� */
	public Quadrangle(Quadrangle_side qs) {
		this.qs = qs;
	}
	/** �������� �������� */
	public int getts()
	{
		return qs.getSide();
	}
	/** ��������� � ����� */
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("qs.bin"));
		os.writeObject(qs);
		os.flush();
		os.close();
		}
	/** ��������������� �� �����*/
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream("qs.bin"));
		qs = (Quadrangle_side)is.readObject();
		is.close();
		}
	/** ��������� ����������. */
	public float calcArea(){
		Area = (float) Math.pow(qs.getSide(),2);
		return Area;
	}
}
