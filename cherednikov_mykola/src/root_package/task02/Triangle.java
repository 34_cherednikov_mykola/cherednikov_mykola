package root_package.task02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/**�������� ���������� ������� ��� ����������*/
public class Triangle {
	/** ��������� ��������� ����������. */
	private float Area;
	/** ������ ������. */
	private Triangle_side ts;
	/** �������������� */
	public Triangle(Triangle_side ts) {
		this.ts = ts;
	}
	/** �������� ��������*/
	public int getts()
	{
		return ts.getSide();
	}
	/**��������� � �����*/
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("ts.bin"));
		os.writeObject(ts);
		os.flush();
		os.close();
		}
	/**��������������� �� �����*/
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream("ts.bin"));
		ts = (Triangle_side)is.readObject();
		is.close();
		}
	/**��������� ����������.*/
	public float calcArea(){
		Area = (float) (((Math.sqrt(3))*Math.pow(ts.getSide(),2))/4);
		return Area;
	}
}
